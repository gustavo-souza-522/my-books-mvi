package com.gustavodev.mybooksmvi.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.gustavodev.mybooksmvi.util.LiveDataCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

  //private const val BASE_URL = "http://vv3hhjir.srv-45-34-12-248.webserverhost.top/"
  //private const val BASE_URL = "https://dog.ceo/api/"
  private const val BASE_URL = "http://192.168.25.2:8080/"

  private val retrofitBuilder: Retrofit.Builder by lazy {
    Retrofit.Builder()
      .baseUrl(BASE_URL)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(LiveDataCallAdapterFactory())

  }

  val apiService: ApiService by lazy {
    retrofitBuilder.build()
      .create(ApiService::class.java)
  }

}