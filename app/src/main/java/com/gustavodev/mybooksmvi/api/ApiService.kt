package com.gustavodev.mybooksmvi.api

import androidx.lifecycle.LiveData
import com.gustavodev.mybooksmvi.domain.Book
import com.gustavodev.mybooksmvi.util.GenericApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

  @GET(value = "book_search.php")
  fun searchBook(@Query("bookName") bookName: String): LiveData<GenericApiResponse<List<Book>>>

}