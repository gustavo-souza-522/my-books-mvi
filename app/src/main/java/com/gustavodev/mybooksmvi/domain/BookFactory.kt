package com.gustavodev.mybooksmvi.domain

object BookFactory {

  fun getSome(): List<Book> {
    val bookList: ArrayList<Book> = ArrayList()

    bookList.add(Book(
      id = "8575227262",
      name = "Dominando o Android com Kotlin",
      author = "Nelson Glauber",
      avaliation = 4.8f,
      imageSrc = "https:\\/\\/m.media-amazon.com\\/images\\/I\\/71e39jzOunL._AC_UY218_.jpg"
    ))
    bookList.add(Book(
      id = "8575224638",
      name = "Dominando o Android: Do B\\u00e1sico ao Avan\\u00e7ado",
      author = "Nelson Glauber",
      avaliation = 4.9f,
      imageSrc = "https:\\/\\/m.media-amazon.com\\/images\\/I\\/71dibiwTOrL._AC_UY218_.jpg"
    ))
    bookList.add(Book(
      id = "8575226894",
      name = "Android Essencial com Kotlin",
      author = "Ricardo R. Lecheta",
      avaliation = 4.8f,
      imageSrc = "https:\\/\\/m.media-amazon.com\\/images\\/I\\/61C9kqYMleL._AC_UY218_.jpg"
    ))

    return bookList
  }

}