package com.gustavodev.mybooksmvi.domain

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Book(

  @Expose
  @SerializedName("id")
  val id: String,

  @Expose
  @SerializedName("name")
  val name: String,

  @Expose
  @SerializedName("author")
  val author: String,

  @Expose
  @SerializedName("image_src")
  val imageSrc: String,

  @Expose
  @SerializedName("aval")
  val avaliation: Float? = null
) {

  override fun toString(): String {
    return "Book(id='$id', name='$name', author='$author', imageSrc='$imageSrc', avaliation=$avaliation)"
  }
}