package com.gustavodev.mybooksmvi.util

/**
 * Used as a wrapper for data that is exposed via a LiveData.
 */
class Event<T>(private val content: T) {

  var hasBeenConsumed: Boolean = false
    private set

  /**
   * Get the content object.
   */
  fun getContent(): T = content

  /**
   * Get the content only once. If this method is called
   * for second or more time, it will return null.
   */
  fun getContentIfNotConsumed(): T? {
    return if(hasBeenConsumed) {
      null
    } else {
      hasBeenConsumed = true
      getContent()
    }
  }

  override fun toString(): String {
    return "Event(content=$content, hasBeenConsumed=$hasBeenConsumed)"
  }


  companion object {

    // We don't want an event if there is no data.
    fun <T> dataEvent(data: T?): Event<T>? {
      data?.let { return Event(it) }
      return null
    }

    // We don't want an event if there is no message.
    fun messageEvent(message: String?): Event<String>? {
      message?.let { return Event(message) }
      return null
    }

  }

}