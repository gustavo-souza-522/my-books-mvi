package com.gustavodev.mybooksmvi.util

data class DataState<T>(var message: Event<String>? = null, var loading: Boolean = false, var data: Event<T>? = null) {

  companion object {

    /**
     * Get an state of type Error, with message and data.
     */
    fun <T> error(message: String): DataState<T> {
      return DataState(message = Event(message), loading = false, data = null)
    }

    /**
     * Get an state of type Loading, with message and data.
     */
    fun <T> loading(isLoading: Boolean): DataState<T> {
      return DataState(loading = isLoading)
    }

    /**
     * Get an state of type Data, with message and data.
     */
    fun <T> data(message: String? = null, data: T? = null): DataState<T> {
      return DataState(message = Event.messageEvent(message), loading = false, data = Event.dataEvent(data))
    }

  }

  override fun toString(): String {
    return "DataState(message=$message, loading=$loading, data=$data)"
  }


}