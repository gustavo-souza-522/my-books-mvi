package com.gustavodev.mybooksmvi.util

import androidx.lifecycle.LiveData
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean

class LiveDataCallAdapter<R>(private val responseType: Type)
  : CallAdapter<R, LiveData<GenericApiResponse<R>>> {

  /**
   * Returns the value type that this adapter uses when converting the HTTP response body to a Java
   * object. For example, the response type for `Call<Repo>` is `Repo`. This type is
   * used to prepare the `call` passed to `#adapt`.
   *
   *
   * Note: This is typically not the same type as the `returnType` provided to this call
   * adapter's factory.
   */
  override fun responseType(): Type = responseType

  /**
   * Returns an instance of `T` which delegates to `call`.
   *
   *
   * For example, given an instance for a hypothetical utility, `Async`, this instance
   * would return a new `Async<R>` which invoked `call` when run.
   *
   * <pre>`
   * &#64;Override
   * public <R> Async<R> adapt(final Call<R> call) {
   * return Async.create(new Callable<Response<R>>() {
   * &#64;Override
   * public Response<R> call() throws Exception {
   * return call.execute();
   * }
   * });
   * }
  `</pre> *
   */
  override fun adapt(call: Call<R>): LiveData<GenericApiResponse<R>> {
    val liveData = object : LiveData<GenericApiResponse<R>>() {

      private var started = AtomicBoolean(false)

      override fun onActive() {
        super.onActive()

        if (started.compareAndSet(false, true)) {

          call.enqueue( object : Callback<R> {

            /**
             * Invoked for a received HTTP response.
             *
             *
             * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
             * Call [Response.isSuccessful] to determine if the response indicates success.
             */
            override fun onResponse(call: Call<R>, response: Response<R>) {
              postValue(GenericApiResponse.create(response))
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<R>, t: Throwable) {
              postValue(GenericApiResponse.create(t))
            }
          })

        }
      }

    }

    return liveData
  }


}