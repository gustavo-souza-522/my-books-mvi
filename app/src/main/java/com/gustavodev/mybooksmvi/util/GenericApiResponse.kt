package com.gustavodev.mybooksmvi.util

import android.util.Log
import retrofit2.Response
import java.net.HttpURLConnection

/**
 * Copied from Architecture components Google Sample.
 *
 * @author Google Sample
 */
sealed class GenericApiResponse<T> {

  companion object {

    private const val TAG = "AppDebug"


    fun <T> create(error: Throwable): ApiErrorResponse<T> {
      return ApiErrorResponse(error.message ?: "Unknown error")
    }

    fun <T> create(response: Response<T>): GenericApiResponse<T> {

      Log.d(TAG, "GenericApiResponse: response code = ${response.code()}")
      Log.d(TAG, "GenericApiResponse: response raw = ${response.raw()}")
      Log.d(TAG, "GenericApiResponse: response message = ${response.message()}")

      // If response was NOT successful.
      if(!response.isSuccessful) {
        val message = response.errorBody()?.toString()
        val errorMessage = if(message.isNullOrEmpty()) response.message() else message
        return ApiErrorResponse(errorMessage ?: "Unknown error")
      }

      // Get the response body.
      val body = response.body()

      return when {
        // For response code 404 (error).
        response.code() == HttpURLConnection.HTTP_NOT_FOUND -> {
          ApiSuccessResponse(body!!)
        }

        // For response code 204 (empty).
        response.code() == HttpURLConnection.HTTP_NO_CONTENT -> {
          ApiEmptyResponse()
        }

        // For response 200, with result.
        else -> {
          ApiSuccessResponse(body!!)
        }
      }
    }

  }

}

class ApiEmptyResponse<T> : GenericApiResponse<T>()

data class ApiSuccessResponse<T>(val body: T) : GenericApiResponse<T>() {}

data class ApiErrorResponse<T>(val errorMessage: String) : GenericApiResponse<T>()