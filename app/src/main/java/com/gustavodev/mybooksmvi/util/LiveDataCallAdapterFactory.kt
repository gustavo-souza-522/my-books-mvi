package com.gustavodev.mybooksmvi.util

import android.util.Log
import androidx.lifecycle.LiveData
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class LiveDataCallAdapterFactory : CallAdapter.Factory() {

  companion object {
    private const val TAG = "AppDebug"
  }

  /**
   * Returns a call adapter for interface methods that return `returnType`, or null if it
   * cannot be handled by this factory.
   */
  override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {

    // Confirm return type must be a LiveData.
    if(getRawType(returnType) != LiveData::class.java) {
      return null
    }

    val observableType = getParameterUpperBound(0, returnType as ParameterizedType)
    val rawObservableType = getRawType(observableType)

    Log.d(TAG, "LiveDataCallAdapterFactory -> observableType = $observableType")
    Log.d(TAG, "LiveDataCallAdapterFactory -> rawObservableType = $rawObservableType")

    // Confirm raw observable type is a GenericApiResponse class.
    if(rawObservableType != GenericApiResponse::class.java) {
      throw IllegalArgumentException("type must be a resource")
    }

    if(observableType !is ParameterizedType) {
      throw IllegalArgumentException("type must be parameterized")
    }

    val bodyType = getParameterUpperBound(0, observableType)
    return LiveDataCallAdapter<Any>(bodyType)
  }
}