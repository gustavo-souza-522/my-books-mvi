package com.gustavodev.mybooksmvi.util

import androidx.lifecycle.LiveData

/**
 * A LiveData class that has null value.
 *
 * It creates a live data and set its value to null.
 *
 * @author GoogleSamples (android-architecture-components)
 */
class AbsentLiveData<T: Any?> private constructor(): LiveData<T>() {

  init {
    // Use post instead of set since this can be created on any thread.
    postValue(null)
  }

  companion object {
    fun <T> create(): LiveData<T> = AbsentLiveData()
  }

}