package com.gustavodev.mybooksmvi.presentation.main

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.gustavodev.mybooksmvi.R
import com.gustavodev.mybooksmvi.domain.Book
import kotlinx.android.synthetic.main.book_list_item.view.*

class BookSearchListAdapter(private val interaction: Interaction? = null)
  : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

  private val diffCallback = object : DiffUtil.ItemCallback<Book>() {

    override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
      return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
      return oldItem == newItem
    }

  }

  private val differ = AsyncListDiffer(this, diffCallback)

  // -----------------------------------------------------------------------------------------------

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

    return BookSearchViewHolder(
      LayoutInflater.from(parent.context).inflate(
        R.layout.book_list_item,
        parent,
        false
      ),
      interaction
    )
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    when (holder) {
      is BookSearchViewHolder -> {
        holder.bind(differ.currentList[position])
      }
    }
  }

  override fun getItemCount(): Int = differ.currentList.size

  fun submitList(list: List<Book>) {
    differ.submitList(list)
  }

  // -----------------------------------------------------------------------------------------------

  class BookSearchViewHolder constructor(
    itemView: View,
    private val interaction: Interaction?
  ) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: Book) = with(itemView) {
      itemView.setOnClickListener {
        interaction?.onItemSelected(adapterPosition, item)
      }

      listitem_book_name.text = item.name
      listitem_book_author.text = item.author
      listitem_book_avaliation.text = item.avaliation?.toString() ?: "No score"

      Glide
        .with(itemView.context)
        .load(item.imageSrc)
        .into(itemView.listitem_book_image)
    }
  }

  interface Interaction {
    fun onItemSelected(position: Int, item: Book)
  }
}