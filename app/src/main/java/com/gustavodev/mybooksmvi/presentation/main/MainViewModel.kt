package com.gustavodev.mybooksmvi.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.gustavodev.mybooksmvi.domain.Book
import com.gustavodev.mybooksmvi.presentation.main.state.MainStateEvent
import com.gustavodev.mybooksmvi.presentation.main.state.MainStateEvent.*
import com.gustavodev.mybooksmvi.presentation.main.state.MainViewState
import com.gustavodev.mybooksmvi.searcher.BookSearcher
import com.gustavodev.mybooksmvi.util.AbsentLiveData
import com.gustavodev.mybooksmvi.util.DataState

class MainViewModel : ViewModel() {

  private val _stateEvent: MutableLiveData<MainStateEvent> by lazy {
    MutableLiveData()
  }
  private val _viewState: MutableLiveData<MainViewState> by lazy {
    MutableLiveData()
  }

  val viewState: LiveData<MainViewState> get() = _viewState
  val dataState: LiveData<DataState<MainViewState>> = Transformations.switchMap(_stateEvent) { stateEvent ->
    when (stateEvent) {
      is GetAllBooksEvent -> {
        BookSearcher.searchBook("")
      }
      is SearchBookEvent -> {
        BookSearcher.searchBook(stateEvent.bookName)

        //AbsentLiveData.create()
      }
      is None -> {
        AbsentLiveData.create()
      }
    }
  }


  private fun getCurrentViewStateOrNew(): MainViewState {
    val value = viewState.value?.let { it } ?: MainViewState()

    return value
  }

  /**
   * Triggers the StateEvent.
   */
  fun setStateEvent(stateEvent: MainStateEvent) {
    _stateEvent.value = stateEvent
  }

  /**
   * Triggers the ViewState.
   */
  fun setBookList(bookList: List<Book>) {
    val update = getCurrentViewStateOrNew()
    update.bookList = bookList

    _viewState.value = update
  }

}