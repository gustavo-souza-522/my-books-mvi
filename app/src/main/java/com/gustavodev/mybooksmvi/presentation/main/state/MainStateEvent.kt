package com.gustavodev.mybooksmvi.presentation.main.state

/**
 * Contains all operations (use cases) that the MainFragment will use.
 */
sealed class MainStateEvent {

  /**
   * An event to get all books with a pre-determined book name.
   */
  class GetAllBooksEvent() : MainStateEvent()

  /**
   * An event to get a book from book name.
   *
   * @param bookName Book name to search
   */
  class SearchBookEvent(val bookName: String): MainStateEvent()

  /**
   * An none event.
   */
  class None : MainStateEvent()

}