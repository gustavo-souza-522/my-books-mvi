package com.gustavodev.mybooksmvi.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gustavodev.mybooksmvi.R
import com.gustavodev.mybooksmvi.domain.Book
import com.gustavodev.mybooksmvi.presentation.main.state.MainStateEvent
import com.gustavodev.mybooksmvi.util.VerticalSpacingItemDecoration

class MainFragment : Fragment(), BookSearchListAdapter.Interaction {

  private val mMainViewModel: MainViewModel by viewModels()

  private lateinit var mBookSearchListAdapter: BookSearchListAdapter

  private lateinit var mButtonSearch: Button
  private lateinit var mProgressBar: ProgressBar

  // -----------------------------------------------------------------------------------------------

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_main, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    return init(view)
  }

  // -----------------------------------------------------------------------------------------------

  private fun init(view: View) {
    registerObservers()

    initWidgets(view)
  }

  private fun initWidgets(view: View) {
    val inputSearch: EditText = view.findViewById(R.id.fragment_main_input_search)
    mButtonSearch = view.findViewById(R.id.fragment_main_button_search)
    mProgressBar = view.findViewById(R.id.fragment_main_progressbar)

    // Button - search.
    mButtonSearch.setOnClickListener {
      val bookName = inputSearch.text.toString()

      triggerSearchBook(bookName)
    }

    // RecyclerView - search results.
    val recyclerView: RecyclerView = view.findViewById(R.id.fragment_main_recyclerview_search_result)

    mBookSearchListAdapter = BookSearchListAdapter(this)

    recyclerView.apply {
      addItemDecoration(VerticalSpacingItemDecoration(8))
      layoutManager = LinearLayoutManager(requireActivity())
      itemAnimator = DefaultItemAnimator()
      adapter = mBookSearchListAdapter
    }
  }

  override fun onItemSelected(position: Int, item: Book) {
    println("DEBUG: Clicked on item: [position: $position] - $item")
  }

  /**
   * Register view model observers.
   */
  private fun registerObservers() {

    // The DataState handle the data coming from a repository, then later
    // will trigger the ViewState to update changes on UI.
    mMainViewModel.dataState.observe(viewLifecycleOwner) { dataState ->
      println("[DEBUG] DataState: $dataState")

      // Handle data.
      dataState.data?.let { event ->
        val bookListData = event.getContentIfNotConsumed()?.bookList

        bookListData?.let { bookList ->

          if(bookList.isNotEmpty()) {
            mMainViewModel.setBookList(bookList)

            // Set BookList to RecyclerView.
            mBookSearchListAdapter.submitList(bookList)
          }
          else {
            Toast.makeText(requireActivity(), "Book not found!", Toast.LENGTH_SHORT).show()
          }

        }
      }

      // Handle message.
      dataState.message?.let { event ->
        event.getContentIfNotConsumed()?.let { message ->
          Toast.makeText(requireActivity(), message, Toast.LENGTH_LONG).show()
        }

      }

      // Handle loading.
      setLoading(dataState.loading)
    }

    // The ViewState catches data from DataState and will update the UI.
    mMainViewModel.viewState.observe(viewLifecycleOwner) { viewState ->
      viewState.bookList?.let {
        println("DEBUG: Setting book list to RecyclerView: $it")
      }
    }
  }

  /**
   * Request a search.
   */
  private fun triggerSearchBook(bookName: String) {
    if (bookName.isEmpty()) mMainViewModel.setStateEvent(MainStateEvent.GetAllBooksEvent())
    else mMainViewModel.setStateEvent(MainStateEvent.SearchBookEvent(bookName))
  }

  private fun setLoading(isLoading: Boolean) {
    if(isLoading) {
      mButtonSearch.isEnabled = false
      mProgressBar.visibility = View.VISIBLE
    } else {
      mButtonSearch.isEnabled = true
      mProgressBar.visibility = View.GONE
    }
  }

}