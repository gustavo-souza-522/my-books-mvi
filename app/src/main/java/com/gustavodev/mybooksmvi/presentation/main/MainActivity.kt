package com.gustavodev.mybooksmvi.presentation.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gustavodev.mybooksmvi.R

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
  }

}