package com.gustavodev.mybooksmvi.presentation.main.state

import com.gustavodev.mybooksmvi.domain.Book

/**
 * Contains all data which will be used on MainFragment.
 *
 * @see [com.gustavodev.mybooksmvi.presentation.main.MainFragment].
 */
data class MainViewState(
  var bookList: List<Book>? = null
)