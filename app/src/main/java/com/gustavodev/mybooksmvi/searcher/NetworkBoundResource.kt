package com.gustavodev.mybooksmvi.searcher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.gustavodev.mybooksmvi.presentation.main.state.MainViewState
import com.gustavodev.mybooksmvi.util.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class NetworkBoundResource<ResponseObject, ViewStateType> {

  protected val result = MediatorLiveData<DataState<ViewStateType>>()


  init {
    result.value = DataState.loading(true)

    GlobalScope.launch(IO) {

      withContext(Main) {
        val apiResponse = createCall()
        result.addSource(apiResponse) { response ->
          result.removeSource(apiResponse)

          handleNetworkCall(response)
        }
      }

    }
  }

  abstract fun createCall(): LiveData<GenericApiResponse<ResponseObject>>

  abstract fun handleApiSuccessResponse(response: ApiSuccessResponse<ResponseObject>)

  abstract fun handleApiEmptyResponse(response: ApiEmptyResponse<ResponseObject>)

  fun handleNetworkCall(response: GenericApiResponse<ResponseObject>) {
    when(response) {
      is ApiSuccessResponse -> {
        handleApiSuccessResponse(response = response)
      }
      is ApiEmptyResponse -> {
        println("DEBUG: NetworkBoundResource: ${response}")
        handleApiEmptyResponse(response = response)
      }
      is ApiErrorResponse -> {
        println("DEBUG: NetworkBoundResource: ${response.errorMessage}")
        onReturnError(message = response.errorMessage)
      }
    }
  }

  fun onReturnError(message: String) {
    result.value = DataState.error(message = message)
  }

  fun asLiveData() = result as LiveData<DataState<ViewStateType>>

}