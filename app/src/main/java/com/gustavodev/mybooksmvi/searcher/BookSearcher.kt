package com.gustavodev.mybooksmvi.searcher

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.gustavodev.mybooksmvi.api.RetrofitBuilder
import com.gustavodev.mybooksmvi.domain.Book
import com.gustavodev.mybooksmvi.presentation.main.state.MainViewState
import com.gustavodev.mybooksmvi.util.*

object BookSearcher {

  fun searchBook(bookName: String): LiveData<DataState<MainViewState>> {
    return object : NetworkBoundResource<List<Book>, MainViewState>() {

      override fun createCall(): LiveData<GenericApiResponse<List<Book>>> {
        return RetrofitBuilder.apiService.searchBook(bookName)
      }

      override fun handleApiSuccessResponse(response: ApiSuccessResponse<List<Book>>) {
        result.value = DataState.data(data = MainViewState(bookList = response.body))
      }

      override fun handleApiEmptyResponse(response: ApiEmptyResponse<List<Book>>) {
        result.value = DataState.data(data = MainViewState(bookList = emptyList()))
      }


    }.asLiveData()
  }

}